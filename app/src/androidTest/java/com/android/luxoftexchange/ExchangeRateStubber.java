package com.android.luxoftexchange;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathMatching;

public class ExchangeRateStubber {

    public static void stubExchangeRateResponseWithError(int errorCode) {
       // BaseUrlChangingInterceptor.get().setInterceptor(ServerConfig.LOCAL_HOST + url);
        stubFor(get(urlPathMatching(ServerConfig.LOCAL_HOST))
                .willReturn(aResponse()
                        .withStatus(errorCode)
                ));
    }

    public static void stubExchangeResponse() {
       // BaseUrlChangingInterceptor.get().setInterceptor(ServerConfig.LOCAL_HOST);
        String jsonBody = AssetReaderUtil.asset("exchange_rate.json");
        stubFor(get(urlPathMatching(ServerConfig.LOCAL_HOST))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withBody(jsonBody)));
    }
}
