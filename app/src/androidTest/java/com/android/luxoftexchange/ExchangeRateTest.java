package com.android.luxoftexchange;

import android.content.Intent;

import androidx.recyclerview.widget.RecyclerView;
import androidx.test.rule.ActivityTestRule;

import com.android.luxoftexchange.model.Rates;
import com.github.tomakehurst.wiremock.junit.WireMockRule;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import static androidx.test.espresso.matcher.ViewMatchers.*;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static org.junit.Assert.*;
import static org.hamcrest.Matchers.allOf;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;


public class ExchangeRateTest {


    @Rule
    public ActivityTestRule<com.android.luxoftexchange.MainActivity> mActivityTestRule = new ActivityTestRule<com.android.luxoftexchange.MainActivity>
            (com.android.luxoftexchange.MainActivity.class, true,
            false);

    @Test
    public void onInternalServerError_showAppropriateMessage() {
        ExchangeRateStubber.stubExchangeRateResponseWithError(501);
        reloadActivity();
        onView(allOf(withId(R.id.error_text_view), withText("Something went wrong"))).check(matches(isDisplayed()));
    }

    @Test
    public void onBadGatewayError_showAppropriateMessage() {
        ExchangeRateStubber.stubExchangeRateResponseWithError(502);
        reloadActivity();
        onView(allOf(withId(R.id.error_text_view), withText("Something went wrong"))).check(matches(isDisplayed()));
    }

    @Test
    public void onAuthError_showAppropriateMessage() {
        ExchangeRateStubber.stubExchangeRateResponseWithError(401);
        reloadActivity();
        onView(allOf(withId(R.id.error_text_view), withText("Login again"))).check(matches(isDisplayed()));
    }

    @Test
    public void onNotFoundError_showAppropriateMessage() {
        ExchangeRateStubber.stubExchangeRateResponseWithError(404);
        reloadActivity();
        onView(allOf(withId(R.id.error_text_view), withText("No resource found"))).check(matches(isDisplayed()));
    }

    @Test
    public void onCorrectResponse_checkExchangeRate() {
      // ExchangeRateStubber.stubExchangeResponse();
        reloadActivity();
       // RecyclerView recyclerView = mActivityTestRule.getActivity().recyclerView;
        String jsonResponse = JsonUtils.getResponseFromJsonFile("exchange_rate.json");
        Rates rates = new Rates();
        JSONObject jsonObject;
        try {
            jsonObject = new JSONObject(jsonResponse);
            JSONObject ratesJSONObject = jsonObject.optJSONObject("rates");
            if(ratesJSONObject != null){
                Iterator keys = ratesJSONObject.keys();
                HashMap<String,String> ratesMapData = new HashMap<>();
                while(keys.hasNext()) {
                    String currentDynamicKey = (String)keys.next();
                    String currentDynamicValue = ratesJSONObject.optString(currentDynamicKey);
                    ratesMapData.put(currentDynamicKey,currentDynamicValue);
                }
                rates.setRatesMap(ratesMapData);
            }
            rates.setBase(jsonObject.optString("base"));
            rates.setDate(jsonObject.optString("date"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        checkBaseExchangeValue(rates, null);
    }

    private void checkBaseExchangeValue(Rates paramRates, RecyclerView recyclerView) {
        try {
            assertEquals("EUR", paramRates.getBase());
            Thread.sleep(10000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void reloadActivity() {
        Intent intent = new Intent();
        mActivityTestRule.launchActivity(intent);
    }

}
