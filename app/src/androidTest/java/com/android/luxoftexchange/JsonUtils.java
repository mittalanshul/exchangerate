package com.android.luxoftexchange;

import android.content.res.AssetManager;
import java.io.IOException;
import java.io.InputStream;

import static com.android.luxoftexchange.AssetReaderUtil.inputStreamToString;

public class JsonUtils {

    public static String getResponseFromJsonFile(String path) {
        AssetManager am = androidx.test.platform.app.InstrumentationRegistry.getInstrumentation().getTargetContext().getAssets();
        try {
            InputStream inputStream = am.open("body/"+path);
            String jsonString = inputStreamToString(inputStream, "UTF-8");
            return jsonString;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
