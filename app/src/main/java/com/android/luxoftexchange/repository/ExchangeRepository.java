package com.android.luxoftexchange.repository;

import com.android.luxoftexchange.constants.Constants;
import com.android.luxoftexchange.network.RetrofitApiService;
import com.google.gson.JsonElement;

import io.reactivex.Observable;

public class ExchangeRepository {

    private static ExchangeRepository exchangeRepository;

    /**
     * get object of repository for getting  access of members
     * @return
     */
    public static ExchangeRepository getInstance()
    {
        if(exchangeRepository==null)
        {
            synchronized (ExchangeRepository.class)
            {
                if(exchangeRepository == null)
                {
                    exchangeRepository = new ExchangeRepository();
                }
            }
        }
        return exchangeRepository;
    }



    public Observable<JsonElement> getAllExchangeList() {
        return RetrofitApiService.create(Constants.BASE_URL).getAllExchangeList();
    }
}
