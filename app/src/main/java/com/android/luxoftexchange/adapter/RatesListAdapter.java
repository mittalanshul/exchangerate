package com.android.luxoftexchange.adapter;

import android.app.Activity;
import android.content.Context;
import android.provider.Telephony;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.luxoftexchange.R;
import com.android.luxoftexchange.model.Rates;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class RatesListAdapter extends RecyclerView.Adapter<RatesListAdapter.ExchangeRateHolder> {

    private HashMap<String,String> mRatesMap;
    private Rates mRates;
    List<String> indexes;

    public RatesListAdapter(Rates paramRates) {
        mRates = paramRates;
        mRatesMap = mRates.getRatesMap();
        indexes = new ArrayList<String>(mRatesMap.keySet());
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ExchangeRateHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        TextView tvCurrencyName, tvCurrencyRate , tvCurrencyBase;
        public ExchangeRateHolder(View v) {
            super(v);
            tvCurrencyName = v.findViewById(R.id.tv_currency);
            tvCurrencyRate = v.findViewById(R.id.tv_rates);
            tvCurrencyBase = v.findViewById(R.id.tv_currency_base);
        }
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RatesListAdapter.ExchangeRateHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        // create a new view
        LinearLayout v = (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_row, parent, false);
        ExchangeRateHolder vh = new ExchangeRateHolder(v);
        return vh;
    }


    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ExchangeRateHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        String key = indexes.get(position);
        holder.tvCurrencyName.setText(key);
        holder.tvCurrencyRate.setText(mRatesMap.get(key));
        holder.tvCurrencyBase.setText(mRates.getBase());

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mRatesMap.size();
    }



}

