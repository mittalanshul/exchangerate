package com.android.luxoftexchange.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class ApiResponse {

    public final Status status;
    @Nullable
    public  Object data;

    @Nullable
    public  Throwable error;

    @Nullable
    public  String msg;

    public ApiResponse(Status status, @Nullable Object data, @Nullable Throwable error) {
        this.status = status;
        this.data = data;
        this.error = error;
    }

    public ApiResponse(Status status,String msg) {
        this.status = status;
        this.msg = msg;
    }

    public static ApiResponse loading() {
        return new ApiResponse(Status.LOADING, null, null);
    }

    public static ApiResponse success(@NonNull Object data) {
        return new ApiResponse(Status.SUCCESS, data, null);
    }

    public static ApiResponse error(@NonNull Throwable error) {

        return new ApiResponse(Status.ERROR, null, error);
    }
    public enum Status {
        LOADING,
        SUCCESS,
        ERROR
    }
}
