package com.android.luxoftexchange.model;

import java.util.HashMap;

public class Rates {

    private HashMap<String ,String> ratesMap;
    private String base;
    private String date;

    public HashMap<String, String> getRatesMap() {
        return ratesMap;
    }

    public void setRatesMap(HashMap<String, String> ratesMap) {
        this.ratesMap = ratesMap;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }



}
