package com.android.luxoftexchange.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.android.luxoftexchange.constants.Constants;
import com.android.luxoftexchange.model.ApiResponse;
import com.android.luxoftexchange.model.Rates;
import com.android.luxoftexchange.repository.ExchangeRepository;
import com.google.gson.JsonElement;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class CurrencyExchangeViewModel extends AndroidViewModel {

    public ExchangeRepository exchangeRepository;
    private MutableLiveData<ApiResponse> exchangeDetailLiveData = new MutableLiveData<>();
    private Disposable disposable;

    public CurrencyExchangeViewModel(@NonNull Application application) {
        super(application);
        exchangeRepository = ExchangeRepository.getInstance();
    }

    public MutableLiveData<ApiResponse> observeExchangeCartDetailLiveData(){
        return exchangeDetailLiveData;
    }

    public void subscribeExchangeRate(){
       if(disposable.isDisposed()){
            disposable = Observable.interval(1000, 5000,
                    TimeUnit.MILLISECONDS)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::getCalls, this::onError);
        }
    }

    public void callExchangeRate(){
            disposable = Observable.interval(1000, Constants.API_TIME_DURATION,
                    TimeUnit.MILLISECONDS)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::getCalls, this::onError);
    }

    public void unSubscribeExchangeRate(){
        disposable.dispose();
    }

    private void onError(Throwable throwable) {

    }

    public void getCalls(Long aLong){
        getExchangeList();
    }

    /**
     * get cart details via API
     */
    public void getExchangeList() {
        exchangeRepository.getAllExchangeList().observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe((d) -> exchangeDetailLiveData.postValue(ApiResponse.loading()))
                .subscribeOn(Schedulers.io())
                .map(new Function<JsonElement, Object>() {

                    @Override
                    public Rates apply(JsonElement jsonElement) throws Exception {
                        Rates rates = new Rates();
                        JSONObject jsonObject = new JSONObject(jsonElement.toString());
                        JSONObject ratesJSONObject = jsonObject.optJSONObject("rates");
                        if(ratesJSONObject != null){
                            Iterator keys = ratesJSONObject.keys();
                            HashMap<String,String> ratesMapData = new HashMap<>();
                            while(keys.hasNext()) {
                                String currentDynamicKey = (String)keys.next();
                                String currentDynamicValue = ratesJSONObject.optString(currentDynamicKey);
                                ratesMapData.put(currentDynamicKey,currentDynamicValue);
                            }
                            rates.setRatesMap(ratesMapData);
                        }
                        rates.setBase(jsonObject.optString("base"));
                        rates.setDate(jsonObject.optString("date"));

                        return rates;
                    }
                })
                .subscribe(
                        result -> exchangeDetailLiveData.postValue(ApiResponse.success(result)),
                        throwable -> exchangeDetailLiveData.postValue(ApiResponse.error(throwable))
                );

    }


}
