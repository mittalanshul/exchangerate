package com.android.luxoftexchange.callbacks;

import com.android.luxoftexchange.constants.Constants;
import com.google.gson.JsonElement;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface RetrofitApiInterface {

    @GET(Constants.EXCHANGE_LIST_URL)
    Observable<JsonElement> getAllExchangeList();
}
