package com.android.luxoftexchange;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import com.android.luxoftexchange.adapter.RatesListAdapter;
import com.android.luxoftexchange.model.ApiResponse;
import com.android.luxoftexchange.model.Rates;
import com.android.luxoftexchange.viewmodel.CurrencyExchangeViewModel;
import com.android.luxoftexchange.workmanager.NotificationWorker;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.annotations.Nullable;

public class MainActivity extends AppCompatActivity {

    protected boolean mDestroyed;
    protected CurrencyExchangeViewModel mCurrencyExchangeViewModel;
    protected Rates mRates;
    protected RecyclerView mRecyclerView;
    protected RatesListAdapter mRatesListAdapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        initToolbar();
        initRecyclerView();
        initView();
    }

    /**
     * This method is used to display the tool bar
     */
    private void initToolbar(){
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    /**
     * This method is used to initialize the activity views
     */
    private void initRecyclerView(){
        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mCurrencyExchangeViewModel.subscribeExchangeRate();

    }

    @Override
    protected void onPause() {
        super.onPause();
        mCurrencyExchangeViewModel.unSubscribeExchangeRate();
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void initView(){
        mCurrencyExchangeViewModel = ViewModelProviders.of(this).get(CurrencyExchangeViewModel.class);
        mCurrencyExchangeViewModel.observeExchangeCartDetailLiveData().observe(this, new Observer<ApiResponse>() {
            @Override
            public void onChanged(@Nullable ApiResponse apiResponse) {
                switch (apiResponse.status) {
                    case LOADING:
                       // progressCircular.setVisibility(View.VISIBLE);
                        break;
                    case SUCCESS:
                        //progressCircular.setVisibility(View.GONE);
                        Log.v("anshul","rates api called ");
                        mRates = (Rates) apiResponse.data;
                        mRatesListAdapter = new RatesListAdapter(mRates);
                        mRecyclerView.setAdapter(mRatesListAdapter);
                        break;
                    case ERROR:
                       // progressCircular.setVisibility(View.GONE);
                        Log.v("anshul","error is ");
                        break;
                }
            }
        });
       // getExchangeRateList();
       mCurrencyExchangeViewModel.callExchangeRate();


        Data data = new Data.Builder().putString("test","test").build();

       final OneTimeWorkRequest oneTimeWorkRequest = new OneTimeWorkRequest.Builder(NotificationWorker.class)
               .setInputData(data)
               .build();



        WorkManager.getInstance().enqueue(oneTimeWorkRequest);

        WorkManager.getInstance().getWorkInfoByIdLiveData(oneTimeWorkRequest.getId())
                .observe(this, new Observer<WorkInfo>() {
                    @Override
                    public void onChanged(WorkInfo workInfo) {
                        Log.v("anshul",workInfo.getState().name());
                    }
                });
    }

    @Override
    protected void onDestroy() {
        mDestroyed = true;
        super.onDestroy();
    }


}
