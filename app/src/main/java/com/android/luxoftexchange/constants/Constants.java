package com.android.luxoftexchange.constants;

public class Constants {

    public static final String BASE_URL = "https://api.exchangeratesapi.io/";
    public static final String EXCHANGE_LIST_URL = "latest";
    public static final int API_TIME_DURATION = 5000; // in milliseconds
}
